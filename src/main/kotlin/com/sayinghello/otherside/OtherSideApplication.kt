package com.sayinghello.otherside

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class OtherSideApplication

fun main(args: Array<String>) {
    runApplication<OtherSideApplication>(*args)
}
