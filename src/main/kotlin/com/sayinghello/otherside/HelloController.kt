package com.sayinghello.otherside

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1")
class HelloController {
    @GetMapping("/hi")
    fun sayHi(): String {
        return "Hey!!!"
    }
}